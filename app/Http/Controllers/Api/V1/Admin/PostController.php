<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Post;
use App\Category;
use App\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use App\Notifications\NewPostNotify;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use App\Http\Resources\PostCollection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Notification;
use App\Tag;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::paginate(6);

        request()->message = 'You have found post successfully';
        request()->code = 200;

        return new PostCollection($posts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:posts|max:255',
            'image' => 'required',
            'categories' => 'required',
            'tags' => 'required',
            'body' => 'required',
        ], [
            'title.required' => __('backend/post.post_title_required'),
            'image.required' => __('backend/post.post_img_required'),
            'categories.required' => __('backend/post.categories_required'),
            'tags.required' => __('backend/post.tags_required'),
            'body.required' => __('backend/post.content_required'),
        ]);

        $image = $request->file('image');
        $slug = str_slug($request->title);

        if (isset($image)) {
            //make unique name for image
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

            if (!Storage::disk('public')->exists('post')) {
                Storage::disk('public')->makeDirectory('post');
            }

            $postImage = Image::make($image)->resize(1600, 1066)->stream();
            Storage::disk('public')->put('post/' . $imageName, $postImage);
        } else {
            $imageName = "default.png";
        }
        $post = new Post();
        $post->user_id = Auth::id();
        $post->title = $request->title;
        $post->slug = $slug;
        $post->image = $imageName;
        $post->body = $request->body;

        if (isset($request->status)) {
            $post->status = true;
        } else {
            $post->status = false;
        }

        
        $post->is_approved = true;
        $post->save();

        $post->categories()->attach($request->categories);
        $post->tags()->attach($request->tags);

        $subscribers = Subscriber::all();
        foreach ($subscribers as $subscriber) {
            Notification::route('mail', $subscriber->email)->notify(new NewPostNotify($post));
        }

        $request->message = 'Add post successfully';
        $request->code = 200;
        return new PostCollection(Post::paginate(6));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       if (Post::where('id', $id)->exists()) {
           $post = Post::where('id', $id)->get();
           return response($post, 200);
       } else {
           return response()->json([
            'message' => 'Post not found!',
           ], 404);
       }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Post::where('id', $id)->exists()) {
            $this->validate($request, [
                'title' => 'required|unique:posts|max:255',
                'image' => 'required',
                'categories' => 'required',
                'tags' => 'required',
                'body' => 'required',
            ], [
                'title.unique' => __('backend/post.post_title_required'),
                'image.required' => __('backend/post.post_img_required'),
                'categories.required' => __('backend/post.categories_required'),
                'tags.required' => __('backend/post.tags_required'),
                'body.required' => __('backend/post.content_required'),
    
            ]);
            $post = Post::find($id);
            $image = $request->file('image');
            $slug = str_slug($request->title);
            if (isset($image)) {
                //make unique name for image
                $currentDate = Carbon::now()->toDateString();
                $imageName = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();
    
                if (!Storage::disk('public')->exists('post')) {
                    Storage::disk('public')->makeDirectory('post');
                }
                //delete old post image
                if (Storage::disk('public')->exists('post/' . $post->image)) {
                    Storage::disk('public')->delete('post/' . $post->image);
                }
    
                $postImage = Image::make($image)->resize(1600, 1066)->stream();
                Storage::disk('public')->put('post/' . $imageName, $postImage);
    
            } else {
                $imageName = $post->image;
            }
    
            $post->user_id = Auth::id();
            $post->title = $request->title;
            $post->slug = $slug;
            $post->image = $imageName;
            $post->body = $request->body;
            if (isset($request->status)) {
                $post->status = true;
            } else {
                $post->status = false;
            }
            $post->is_approved = true;
            $post->save();
    
            $post->categories()->sync($request->categories);
            $post->tags()->sync($request->tags);

            return response()->json([
                'message' => 'Post updated successfully',
            ], 200);
        } else {
            return response()->json([
                'message' => 'Post not found',
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Post::where('id', $id)->exists()) {
            $post = Post::find($id);
            $post->delete();

            return response()->json([
                'message' => 'Post was deleted from records',
            ], 202);
        } else {
            return response()->json([
                'message' => 'Post not found!',
            ], 404);
        }
    }
}
