<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Comment;
use App\Http\Controllers\Controller;
use App\Http\Resources\CommentCollection;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::paginate(6);

        request()->message = 'Comments found successfully';
        request()->code = 200;

        return new CommentCollection($comments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Comment::where('id', $id)->exists()) {
            $comment = Comment::where('id', $id)->get();

            return response($comment, 200);
        } else {
            return response()->json([
                'message' => 'Comment not found!',
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Comment::where('id', $id)->exists()) {
            $comment = Comment::find($id);
            $comment->delete();

            return response()->json([
                'message' => 'Comment deleted successfully',
            ], 202);
        } else {
            return response()->json([
                'message' => 'Comment not found!',
            ], 404);
        }
    }
}
