<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Tag;
use Illuminate\Http\Request;
use App\Http\Resources\TagResource;
use App\Http\Controllers\Controller;
use App\Http\Resources\TagCollection;
use Illuminate\Support\Facades\Validator;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::paginate(6);

        request() ->message = 'You have found tags successfully.';
        request() ->code = 200;

        return new TagCollection($tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ],[
            'name.required' => __('backend/tag.tag_required'),
        ]);
        $tag = new \App\Tag();
        $tag->name = $request->name;
        $tag->slug = str_slug($request->name);
        $tag->save();

        $request->message = 'Add tag Successfully';
        $request->code = 200;

        return new TagCollection(Tag::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Tag::where('id', $id)->exists()) {
            $tag = Tag::where('id', $id)->get();
            return response($tag, 200);
        } else {
            return response()->json([
                'message' => 'Tag not found!',
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Tag::where('id', $id)->exists()) {
           $tag = Tag::find($id);
           $tag->name = is_null($request->name) ?$tag->name : $request->name;
           $tag->slug = str_slug($request->name);
           $tag->save();
    
            return response()->json([
                'message' => 'Tag updated successfully',
            ], 200);
            } else {
            return response()->json([
                'message' => 'Tag not found'
            ], 404);
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Tag::where('id', $id)->exists()) {
            $tag = Tag::find($id);
            $tag->delete();

            return response()->json([
                'message' => 'Tag was deleted from records',
            ], 202);
        } else {
            return response()->json([
                'message' => 'Tag not found!',
            ], 404);
        }
    }
}
