<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\SubscriberCollection;
use App\Subscriber;
use Illuminate\Http\Request;

class SubscriberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscribers = Subscriber::paginate(6);
        
        request()->message = 'You have found subscribers successfully';
        request()->code = 200;

        return new SubscriberCollection($subscribers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Subscriber::where('id', $id)->exists()) {
            $subscriber = Subscriber::where('id', $id)->get();
            
            return response($subscriber, 200);
        } else {
            return response()->json([
                'message' => 'Subscriber not found!',
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Subscriber::where('id', $id)->exists()) {
            $subscriber = Subscriber::find($id);
            $subscriber->delete();

            return response()->json([
                'message' => 'Subscriber was deleted successfully',
            ], 202);
        } else {
            return response()->json([
                'message' => 'Subscriber not found!',
            ], 404);
        }
    }
}
