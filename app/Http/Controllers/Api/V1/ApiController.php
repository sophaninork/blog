<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function sendResponse($status, $message, $result)
    {
        $response = [
          'status' => $status,
          'message' => $message,
          'data' => $result,
        ];

        return response()->json($response, 200);
    }

    public function sendError($status, $message, $code=404)
    {
        $response = [
            'status' => $status,
            'message' => $message,
            'code' => $code,
        ];

        return response()->json($response, $code);
    }
}
