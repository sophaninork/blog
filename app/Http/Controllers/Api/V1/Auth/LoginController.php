<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Http\Controllers\Api\V1\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends ApiController
{
    public function login(Request $request)
    {
        $validation = Validator::make($request->all(), [
           'email' => 'required|email',
           'password' => 'required',
        ]);

        if ($validation->fails())
        {
            return $this->sendError(false, $validation->messages(), 500);
        }

        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        if (!Auth::attempt($credentials)){
            return $this->sendError(false, 'You are unauthorized', 401);
        }

        $token = Auth::user()->createToken('Blog.com')->accessToken;

        $response_success = [
            'user' => Auth::user(),
            'access_token' => $token,
        ];

        return $this->sendResponse(true, 'You are Login Successfully', $response_success);

    }
}
