<?php

namespace App\Http\Controllers\Api\V1\Author;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use App\Http\Resources\AuthCollection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\V1\ApiController;

class AuthController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authors = User::authors()
            ->withCount('posts')
            ->withCount('comments')
            ->withCount('favorite_posts')
            ->paginate(6);

            request()->message = 'You have found author successfully.';
            request()->code = 200;

        return new AuthCollection($authors);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed'
        ]);

        if ($validation->fails()){
            return $this->sendError(false, $validation->messages(), 500);
        }

        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        $user = User::create($data);
        $token = $user->createToken('Blog.com')->accessToken;

        return $this->sendResponse(true, 'Use is added successfully', $token);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (User::where('id', $id)->exists()) {
            $author = User::where('id', $id)->get();
            return response($author, 200);
        } else {
            return response()->json([
                'message' => 'Author not found!',
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (User::where('id', $id)->exists()) {
            
            $this->validate($request,[
                'name' => 'required|string|max:255',
                'dob' =>'required',
                'phone_number' => 'required',
                'address' => 'required|min:10|max:255',
                'postal_code' => 'required|min:4|max:6',
                'identify_type' => 'required|min:10|integer',
                'email' => 'required|email|unique:users',
                'image' => 'image',
            ],[
                'name.required' => 'The name field is required.',
                'dob.required' => 'The date of birth field is required.',
                'phone_number.required' => 'The phone number field is required.',
                'address.required' => 'The address field is required.',
                'postal_code.required' => 'The zip code field is required.',
                'identify_type.required' => 'The ID field is required.',
                'email.required' => 'The email field is required.',
                'image.required' => 'The image field is required.',
            ]);
            $image = $request->file('image');
            $slug = str_slug($request->name);
            $user = User::findOrFail(Auth::id());
            if (isset($image))
            {
                $currentDate = Carbon::now()->toDateString();
                $imageName = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
                if (!Storage::disk('public')->exists('profile'))
                {
                 Storage::disk('public')->makeDirectory('profile');
                }
    //            Delete old image form profile folder
                if (Storage::disk('public')->exists('profile/'.$user->image))
                {
                    Storage::disk('public')->delete('profile/'.$user->image);
                }
                $profile = Image::make($image)->resize(500,500)->stream();
                Storage::disk('public')->put('profile/'.$imageName,$profile);
            } else {
                $imageName = $user->image;
            }

            $user = User::find($id);
            $user->name = $request->name;
            $user->gender = $request->input('gender');
            $user->dob = $request->dob;
            $user->phone_number = $request->phone_number;
            $user->address = $request->address;
            $user->postal_code = $request->postal_code;
            $user->identify_type = $request->identify_type;
            $user->email = $request->email;
            $user->image = $imageName;
            $user->bio = $request->bio;
            $user->save();

            return response()->json([
                'message' => 'User update successfully',   
            ], 200);
        } else {
            return response()->json([
                'message' => 'User not found!',
            ], 404);
        }
    }

    public function updatePassword(Request $request, $id) 
    {
        if (User::where('id', $id)->exists()) {
            
            $this->validate($request, [
                'old_password' => 'required',
                'password' => 'required|min:5|confirmed|different:old_password',
            ], [
                'old_password.required' => 'Old password field is required',
                'password.required' => 'New password or second field field is required',
            ]);
            
            $hashedPassword = Auth::user()->password;
            if (Hash::check($request->old_password, $hashedPassword)) {
                if (!Hash::check($request->password, $hashedPassword)) {
                    $user = User::find(Auth::id());
                    $user->password = Hash::make($request->password);
                    $user->save();
                } else {
                    return response()->json([
                        'message' => 'Current password does not match!'
                    ], 404);
                }
            } else {
                return response()->json([
                    'message' => 'Current password does not match!'
                ], 404);
            }

            return response()->json([
                'message' => 'Password updated successfully',
            ], 202);
        } else {
            return response()->json([
                'message' => 'User not found!'
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (User::where('id', $id)->exists()) {
            $author = User::find($id);
            $author->delete();

            return response()->json([
                'message' => 'Author was deleted from records',
            ], 202);
        } else {
            return response()->json([
                'message' => 'Author not found!',
            ], 404);
        }
    }
}
