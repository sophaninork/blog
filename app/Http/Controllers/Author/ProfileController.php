<?php

namespace App\Http\Controllers\Author;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;

class ProfileController extends Controller
{
    public function profile($username)
    {
      $author = User::where('username', $username)->first();
      $posts = $author->posts()->approved()->published()->get();
      return view('author.profile', compact('author', 'posts'));
    }
}