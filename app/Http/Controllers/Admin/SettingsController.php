<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class SettingsController extends Controller
{
    public function index()
    {
        return view('admin.settings');
    }

    public function updateProfile(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'gender' => 'required',
            'dob' => 'required',
            'phone_number' => 'required',
            'address' => 'required|min:10|max:255',
            'postal_code' => 'required|min:4|max:6',
            'identify_type' => 'required|min:10|integer',
            'email' => 'required|email',
            'image' => 'image',
        ], [
            'name.required' => 'Name field is required',
            'dob.required' => 'Date of birth field is required',
            'phone_number.required' => 'Phone number field is required',
            'address.required' => 'Address field is required',
            'postal_code.required' => 'Zip code field is required',
            'identify_type.required' => 'ID field is required',
            'email.required' => 'Email field is required',
            'image.required' => 'Image field is required',
        ]);
        $image = $request->file('image');
        $slug = str_slug($request->name);
        $user = User::findOrFail(Auth::id());
        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!Storage::disk('public')->exists('profile')) {
                Storage::disk('public')->makeDirectory('profile');
            }
//            Delete old image form profile folder
            if (Storage::disk('public')->exists('profile/' . $user->image)) {
                Storage::disk('public')->delete('profile/' . $user->image);
            }
            $profile = Image::make($image)->resize(500, 500)->stream();
            Storage::disk('public')->put('profile/' . $imageName, $profile);
        } else {
            $imageName = $user->image;
        }
        $user->name = $request->name;
        $user->gender = $request->input('gender');
        $user->dob = $request->dob;
        $user->phone_number = $request->phone_number;
        $user->address = $request->address;
        $user->postal_code = $request->postal_code;
        $user->identify_type = $request->identify_type;
        $user->email = $request->email;
        $user->image = $imageName;
        $user->bio = $request->bio;
        $user->save();

        return redirect()->back()->with('success', 'Profile updated!');
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|min:5|confirmed|different:old_password',
        ], [
            'old_password.required' => 'Old password field is required',
            'password.required' => 'New password or second field field is required',
        ]);

        $hashedPassword = Auth::user()->password;
        if (Hash::check($request->old_password, $hashedPassword)) {
            if (!Hash::check($request->password, $hashedPassword)) {
                $user = User::find(Auth::id());
                $user->password = Hash::make($request->password);
                $user->save();
                Auth::logout();
                return redirect()->back();
            } else {
                return redirect()->back();
            }
        } else {
            return redirect()->back();
        }

    }
}
