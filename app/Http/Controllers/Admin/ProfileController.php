<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;

class ProfileController extends Controller
{
    public function index()
    {
        // $posts = Post::latest()->approved()->published()->get();
        $posts = [];
        $admin_role = Role::where('name', 'admin')->first();
        $admins = User::where('role_id', $admin_role->id)->get();
        foreach($admins as $admin) {
            $admin_posts = $admin->posts;
            foreach($admin_posts as $admin_post) {
                array_push($posts, $admin_post);
                // $posts = Post::latest()->get();

            }
        }

        return view('admin.profile', compact('posts'));
    }
}
