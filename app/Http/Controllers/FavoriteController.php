<?php

namespace App\Http\Controllers;

use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller
{
    public function add($post)
    {
        $user = Auth::user();
        $isFavorite = $user->favorite_posts()->where('post_id', $post)->count();

        if ($isFavorite == 0)
        {
            $user->favorite_posts()->attach($post);
            Toastr::success(__('frontend/posts.add_to_fav'), 'Success');
            return redirect()->back()->with('success', 'Has been add to your favorite list!');
        } else {
            $user->favorite_posts()->detach($post);
            Toastr::success(__('frontend/posts.remove_from_fav'), 'Success');
            return redirect()->back()->with('delete', __('backend/favorite.delete_success'));
        }
    }
}
