<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->default(2);
            $table->string('name');
            $table->string('username')->unique();
            $table->string('gender')->nullable();
            $table->timestamp('dob')->nullable();
            $table->string('email')->unique();
            $table->string('phone_number', 20)->nullable();
            $table->string('address')->nullable();
            $table->string('postal_code')->nullable();
            $table->integer('identify_type')->nullable();
            $table->string('password');
            $table->string('image')->default('avatar.jpg');
            $table->text('bio')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
