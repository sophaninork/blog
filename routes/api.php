<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user()->paginate(6);
});

Route::post('/v1/auth/register', 'Api\V1\Auth\RegisterController@register')->name('api.v1.auth.register');
Route::post('/v1/auth/login', 'Api\V1\Auth\LoginController@login')->name('api.v1.auth.login');

Route::group(['as'=>'api.v1.admin.', 'prefix' => 'v1/admin', 'middleware' => 'auth:api'], function() {
    Route::post('category', 'Api\V1\Admin\CategoryController@store')->name('category.store');
    Route::get('category', 'Api\V1\Admin\CategoryController@index')->name('category.index');
    Route::post('category/{id}', 'Api\V1\Admin\CategoryController@update')->name('category.update');
    Route::get('category/{id}', 'Api\V1\Admin\CategoryController@show')->name('category.show');
    Route::delete('category/{id}', 'Api\V1\Admin\CategoryController@destroy')->name('category.destroy');

    Route::apiResource('tag', 'Api\V1\Admin\TagController');

    Route::post('post', 'Api\V1\Admin\PostController@store')->name('post.store');
    Route::get('post', 'Api\V1\Admin\PostController@index')->name('post.index');
    Route::post('post/{id}', 'Api\V1\Admin\PostController@update')->name('post.update');
    Route::get('post/{id}', 'Api\V1\Admin\PostController@show')->name('post.show');
    Route::delete('post/{id}', 'Api\V1\Admin\PostController@destroy')->name('post.destroy');

    Route::apiResource('subscriber', 'Api\V1\Admin\SubscriberController');

    Route::apiResource('comment', 'Api\V1\Admin\CommentController');
});

//this route is show list of author, create author, delete author, update profile author, and show author of each id
Route::group(['as'=>'api.v1.', 'prefix'=>'v1/author', 'middleware'=>'auth:api'], function() {
    Route::post('create', 'Api\V1\Author\AuthController@store')->name('auth.store');
    Route::get('index', 'Api\V1\Author\AuthController@index')->name('auth.index');
    Route::post('update/{id}', 'Api\V1\Author\AuthController@update')->name('auth.update');
    Route::get('show/{id}', 'Api\V1\Author\AuthController@show')->name('auth.show');
    Route::delete('destroy/{id}', 'Api\V1\Author\AuthController@destroy')->name('auth.destroy');

    Route::post('password/{id}', 'Api\V1\Author\AuthController@updatePassword')->name('auth.updateP');
});
