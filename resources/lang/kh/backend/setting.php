<?php
return [
    //settings lang
    'profile_settings' => 'ការកំណត់​គណនី',
    'name' => 'ឈ្មោះ',
    'enter_your_name' => 'បញ្ចូលឈ្មោះអ្នក',
    'gender' => 'ភេទ',
    'male' => 'ប្រុស',
    'female' => 'ស្រី',
    'dob' => 'ថ្ងៃខែ​ឆ្នាំ​កំណើត',
    'phone_number' => 'លេខទូរសព្ទ',
    'enter_your_phone_number' => 'បញ្ចូលលេខទូរស័ព្ទរបស់អ្នក',
    'address' => 'អាសយដ្ឋាន',
    'enter_your_address' => 'បញ្ចូលអាសយដ្ឋានរបស់អ្នក',
    'postal_code' => 'លេខ​កូដ​ប្រៃ​ស​ណី​យ',
    'enter_your_postal_code' => 'បញ្ចូលលេខកូដប្រៃសណីយ៍របស់អ្នក',
    'identify_number' => 'កំណត់លេខ',
    'enter_your_identify_code' => 'បញ្ចូលលេខអត្តសញ្ញាណរបស់អ្នក',
    'email_address' => 'អាស័យ​ដ្ឋាន​អ៊ី​ម៉េ​ល',
    'enter_email_address' => 'បញ្ចូលអាសយដ្ឋានអ៊ីមែលរបស់អ្នក',
    'profile_picture' => 'រូបភាពប្រវត្តិរូប',
    'bio' => 'ផ្សេងៗអំពីអ្នក',
    'write_text_here' => 'សរសេរអត្ថបទនៅទីនេះ...',
    'password_setting' => 'ការកំណត់ពាក្យសម្ងាត់',
    'old_pass' => 'ពាក្យសំងាត់​បច្ចុប្បន្ន',
    'enter_current_pass' => 'បញ្ចូលពាក្យសម្ងាត់បច្ចុប្បន្នរបស់អ្នក',
    'new_pass' => 'ពាក្យសម្ងាត់​ថ្មី',
    'enter_new_pass' => 'បញ្ចូលពាក្យសម្ងាត់ថ្មីរបស់អ្នក',
    'pass' => 'ពាក្យសម្ងាត់',
    'enter_new_pass_again' => 'បញ្ចូលពាក្យសម្ងាត់ថ្មីរបស់អ្នកម្តងទៀត',

    'update' => 'រក្សាទុក',
    'refresh' => 'សាជាថ្មី'
];
