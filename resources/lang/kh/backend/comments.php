<?php
return[
    //comments lang
    'all_comments' => 'យោបល់ទាំងអស់',
    'comment_info' => 'ព័ត៌មានអត្ថាធិប្បាយ',
    'post_info' => 'ព័ត៌មានប្រកាស',
    'action' => 'សកម្មភាព',
    'reply_comment' => 'ឆ្លើយតប',
    'delete_comment' => 'មតិយោបល់ត្រូវបានលុបចោល'
];
