<?php
return [
    //subscriber lang
    'all_subscriber' => 'អតិថិជនទាំងអស់',
    'id' => 'លេខសំគាល់',
    'email' => 'អុីម៉ែល',
    'create_at' => 'បង្កើតនៅពេល',
    'update_at' => 'ធ្វើបច្ចុប្បន្នភាពនៅ',
    'action' => 'សកម្មភាព',
    'delete_subscriber' => 'អតិថិជនត្រូវបានលុបចោលដោយជោគជ័យ'
];
