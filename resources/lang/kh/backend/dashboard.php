<?php

return [
      //Dashboard Lang
      'dashboard' => 'ផ្ទាំងព័ត៌មាន',
      'total_posts' => 'ការប្រកាសសរុប',
      'total_favorites' => 'ការចូលចិត្តសរុប',
      'pending_posts' => 'រង់ចាំការប្រកាសសរុប',
      'total_views' => 'ចំនួនមើលសរុប',
      'categories' => 'ប្រភេទសរុប',
      'tags' => 'ថេកសរុប',
      'total_author' => 'អ្នកនិពន្ធសរុប',
      'today_subscribers' => 'អតិថិជនថ្ងៃនេះមានចំនួន',
      'most_popular_post' => 'ប្រកាសពេញនិយមបំផុត',
      'rank' => 'ចំណាត់ថ្នាក់',
      'title' => 'ចំណងជើង',
      'author' => 'អ្នកនិពន្ធ',
      'views' => 'ចំនួនអ្នកមើល',
      'view' => 'មើល',
      'published' => 'បានផ្សព្វផ្សាយ',
      'pending' => 'កំពុងរង់ចាំ',
      'favorites' => 'ការចូលចិត្ត',
      'comments' => 'យោបល់',
      'status' => 'ស្ថានភាព',
      'action' => 'សកម្មភាព',
      'top_10_active_author' => 'អ្នកនិពន្ធសកម្មទាំង ១០ រូប',
      'top_5_popular_posts' => 'កំពូលអ្នកផ្សព្វផ្សាយទាំង ៥',
      'rank_list' => 'បញ្ជីចំណាត់ថ្នាក់',
      'name' => 'ឈ្មោះ',
      'posts' => 'ប្រកាស'
];
