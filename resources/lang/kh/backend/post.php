<?php
return [
    //Index Lang
    'add_new_post' => 'បន្ថែមប្រកាសថ្មី',
    'all_posts' => 'រាល់ប្រកាស',
    'id' => 'លេខសម្គាល់',
    'title' => 'ចំណងជើង',
    'author' => 'អ្នកនិពន្ធ',
    'is_approved' => 'ត្រូវបានអនុម័ត',
    'status' => 'ស្ថានភាព',
    'create_at' => 'បង្កើតនៅ',
    'update_at' => 'ធ្វើបច្ចុប្បន្នភាពនៅ',
    'action' => 'សកម្មភាព',

    //create Lang
    'post_title' => 'ចំណងជើងប្រកាស',
    'featured_image' => 'រូបភាពលក្ខណៈពិសេស',
    'publish' => 'ផ្សព្វផ្សាយ',
    'back' => 'ត្រឡប់មកវិញ',
    'save' => 'រក្សាទុក',
    'category_and_tags' => 'ប្រភេទនិងថេក',
    'select_category' => 'ជ្រើសរើសប្រភេទ',
    'select_tag' => 'ជ្រើសថេក',
    'content' => 'មាតិកា',
    'post_title_required' => 'សូមធ្វើការបញ្ជលឈ្មោះ',
    'post_img_required' => 'សូមធ្វើការបញ្ជលរូមភាព',
    'categories_required' => 'សូមជ្រើសរើសប្រភេទ',
    'content_required' => 'ត្រូវបំពេញវាលមាតិកា',
    'tags_required' => 'សូមជ្រើសរើសថេក',
    'post_create_success' => 'បានជោកជយ័',
    'approved' => 'អនុម័ត',
    'pending' => 'រង់ចាំ',

    //edit Lang
    'edit_post' => 'កែសម្រួលប្រកាស',
    'post_update_success' => 'ធ្វើបច្ចុប្បន្នភាពបានជោកជយ័',

    //destroy lang
    'post_delete_success' => 'លុបបានជោកជយ័'

];
