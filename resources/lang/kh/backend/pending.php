<?php
return [
    //pending posts lang
    'all_pending_posts' => 'រាល់ប្រកាសដែលមិនទាន់សម្រេច',
    'id' => 'លេខសម្គាល់',
    'title' => 'ចំណងជើង',
    'author' => 'អ្នកនិពន្ធ',
    'is_approved' => 'ត្រូវបានអនុម័ត',
    'status' => 'ស្ថានភាព',
    'create_at' => 'បង្កើតនៅ',
    'update_at' => 'ធ្វើបច្ចុប្បន្នភាពនៅ',
    'action' => 'សកម្មភាព',

];
