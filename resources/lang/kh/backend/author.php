<?php
return [
    //autjor lang
    'all_authors' => 'អ្នកនិពន្ធទាំងអស់',
    'id' => 'លេខសំគាល់',
    'name' => 'ឈ្មោះ',
    'post' => 'ប្រកាស',
    'comment' => 'មតិ',
    'favorite_post' => 'ចំនូននៃការចូលចិត្ត',
    'create_at' => 'ត្រូវបានបង្កើតនៅពេល',
    'action' => 'សម្មភាព',
    'delete_author' => 'អ្នកនិពន្ទត្រូវបានលុបចេញពីបញ្ជី'
];
