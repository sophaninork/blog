<?php

return [
      //Index Tags Lang
      'add_new_tag' => 'បន្ថែមស្លាកថ្មី',
      'all_tags' => 'ថេកទាំងអស់',
      'id' => 'លេខសម្គាល់',
      'name' => 'ឈ្មោះ',
      'post_count' => 'ចំនួនប្រកាស',
      'create_at' => 'បង្កើតនៅ',
      'update_at' => 'ធ្វើបច្ចុប្បន្នភាពនៅ',
      'action' => 'សកម្មភាព',

      //Create Tags Lang
      'tag_name' => 'បញ្ជូលឈ្មោះថេក',
      'back' => 'ថយក្រោយ',
      'save' => 'រក្សាទុក',
      'tag_required' => 'សូមបំពេញថេក',
      'tag_create_success' => 'បន្ថែមស្លាកថ្មីជោកជយ័!',

      //Edit Tags Lang
      'edit_tag' => 'កែសម្រួលថេក',
      'tag_name' => 'ឈ្មោះថេក',
      'tag_update_success' => 'ថេកកែរប្រែជោកជយ័!',

      //Destroy Tags Lang
      'tag_delete_success' => 'ថេកត្រូវបានលុប!',
];