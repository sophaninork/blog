<?php
return [
    //favorite posts lang
    'all_favorite_post' => 'រាល់ប្រកាសដែលខ្ញុំចូលចិត្ត',
    'id' => 'លេខសម្គាល់',
    'title' => 'ចំណងជើង',
    'author' => 'អ្នកនិពន្ធ',
    'action' => 'សកម្មភាព',
    'delete_success' => 'ត្រូវបានលុបពីការចូលចិត្តជោកជយ័!'
];
