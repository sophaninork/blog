<?php

return [
    //Sidebar Lang
    'dashboard' => 'ផ្ទាំងព័ត៌មាន',
    'tag' => 'ថេក',
    'category' => 'ប្រភេទ',
    'posts' => 'ប្រកាស',
    'pending_post' => 'រងចាំការប្រកាស',
    'favorite_posts' => 'ប្រកាសដែលចូលចិត្ត',
    'comments' => 'យោបល់',
    'authors' => 'អ្នកនិពន្ធ',
    'subscribers' =>
    'អតិថិជន',
    'settings' => 'ការកំណត់',
    'logout' => 'ចាកចេញ',
    'languages' => 'ភាសា',
    'profile' => 'ប្រវត្តិរូប',
    'sign_out' => 'ចាកចេញ'
];
