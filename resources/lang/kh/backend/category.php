<?php
return [
      //Index Category Lang
      'add_new_category' => 'បន្ថែមប្រភេទថ្មី',
      'all_category' => 'គ្រប់ប្រភេទ',
      'id' => 'លេខសម្គាល់',
      'name' => 'ឈ្មោះ',
      'post_count' => 'ចំនួនប្រកាស',
      'create_at' => 'បង្កើតនៅ',
      'update_at' => 'ធ្វើបច្ចុប្បន្នភាពនៅ',
      'action' => 'សកម្មភាព',

      //Create Category Lang
      'category_name' => 'ឈ្មោះប្រភេទ',
      'back' => 'ត្រឡប់មកវិញ',
      'save' => 'រក្សាទុក',
      'category_name_required' => 'វាលឈ្មោះប្រភេទត្រូវបានទាមទារ',
      'category_img_required' => 'បញ្ជូលរូបភាពត្រូវបានទាមទារ',
      'category_create_success' => 'ប្រភេទបង្កើតដោយជោគជ័យ',

      //Edit category Lang
      'edit_category' => 'Edit Category',
      'category_update_success' => 'ការធ្វើបច្ចុប្បន្នភាពប្រភេទបានជោគជ័យ',

      //Destroy category lang
      'category_delete_success' => 'ប្រភេទលុបដោយជោគជ័យ',
];
