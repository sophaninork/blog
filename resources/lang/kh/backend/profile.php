<?php
return [
    //profile lang

    'posts' => 'ប្រកាស',
    'favorite_posts' => 'ប្រកាសដែលខ្ញុំចូលចិត្ត',
    'about_me' => 'អំពី​ខ្ញុំ',
    'contact' => 'ទំនាក់ទំនង',
    'username' => 'ឈ្មោះ​អ្នកប្រើប្រាស់',
    'email' => 'អ៊ីមែល',
    'tel' => 'ទូរស័ព្ទ',
    'location' => 'ទីតាំង',
    'bio' => 'ផ្សេងៗអំពីខ្ញំុ',
    'timeline' => 'ថាមលាញ',
    'shared_publicly' => 'ចែករំលែកជាសាធារណៈ'

];
