<?php
return [
    //footer lang
    'language' => 'ភាសា',
    'categories' => 'ប្រភេទ',
    'subscribers' => 'អតិថិជន',
    'enter_your_email' => 'បញ្ជូលអីម៉ែលរបស់អ្នកនៅទីនេះ',
    'sub_success' => 'អ្នកបានបន្ថែមដោយជោគជ័យទៅក្នុងបញ្ជីអតិថិជនរបស់យើង :)',
    'email_sub_unique' => 'អ៊ីមែលត្រូវបានគេយករួចហើយ!',
];
