<?php

return [
      //Dashboard Lang
      'dashboard' => 'Dashboard',
      'total_posts' => 'TOTAL POSTS',
      'total_favorites' => 'TOTAL FAVORITES',
      'pending_posts' => 'PENDING POSTS',
      'total_views' => 'TOTAL VIEWS',
      'categories' => 'CATEGORIES',
      'tags' => 'TAGS',
      'total_author' => 'TOTAL AUTHOR',
      'today_subscribers' => 'TODAY SUBSCRIBERS',
      'most_popular_post' => 'MOST POPULAR POST',
      'rank' => 'Rank',
      'title' => 'Title',
      'author' => 'Author',
      'views' => 'Views',
      'view' => 'View',
      'published' => 'Published',
      'pending' => 'Pending',
      'favorites' => 'Favorites',
      'comments' => 'Comments',
      'status' => 'Status',
      'action' => 'Action',
      'top_10_active_author' => 'TOP 10 ACTIVE AUTHOR',
      'top_5_popular_posts' => 'TOP 5 POPULAR POSTS',
      'rank_list' => 'Rank List',
      'name' => 'Name',
      'posts' => 'Posts'
];
