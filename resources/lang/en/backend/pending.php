<?php
return [
    //pending posts lang
    'all_pending_posts' => 'All Pending Posts',
    'id' => 'ID',
    'title' => 'Title',
    'author' => 'Author',
    'is_approved' => 'Is Approved',
    'status' => 'Status',
    'create_at' => 'Create At',
    'update_at' => 'Update At',
    'action' => 'Action',

];
