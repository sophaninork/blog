<?php

return [
      //Index Tags Lang
      'add_new_tag' => 'Add New Tag',
      'all_tags' => 'All TAGS',
      'id' => 'ID',
      'name' => 'Name',
      'post_count' => 'Post Count',
      'create_at' => 'Create At',
      'update_at' => 'Update At',
      'action' => 'Action',

      //Create Tags Lang
      'tag_name' => 'Tag name',
      'back' => 'Back',
      'save' => 'Save',
      'tag_required' => 'Tag field is required',
      'tag_create_success' => 'Tag created success!',

      //Edit Tags Lang
      'edit_tag' => 'Edit Tag',
      'tag_name' => 'Tag Name',
      'tag_update_success' => 'Update successfully!',

      //Destroy Tags Lang
      'tag_delete_success' => 'Delete successfully!',
];