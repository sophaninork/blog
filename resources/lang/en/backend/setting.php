<?php
return [
    //settings lang
    'profile_settings' => 'Profile settings',
    'name' => 'Name',
    'enter_your_name' => 'Enter Your Name',
    'gender' => 'Gender',
    'male' => 'Male',
    'female' => 'Female',
    'dob' => 'Date of Birth',
    'phone_number' => 'Phone Number',
    'enter_your_phone_number' => 'Enter Your Phone Number',
    'address' => 'Address',
    'enter_your_address' => 'Enter Your Address',
    'postal_code' => 'Postal Code',
    'enter_your_postal_code' => 'Enter Your Postal Code',
    'identify_number' => 'Identify Number',
    'enter_your_identify_code' => 'Enter Your Identify Code',
    'email_address' => 'E-mail Address',
    'enter_email_address' => 'Enter Your E-mail Address',
    'profile_picture' => 'Profile Picture',
    'bio' => 'Bio',
    'write_text_here' => 'Write text here...',

    'password_setting' => 'Password Settings',
    'old_pass' => 'Current Password',
    'enter_current_pass' => 'Enter Your Current Password',
    'new_pass' => 'New Password',
    'enter_new_pass' => 'Enter Your New Password',
    'pass' => 'Password',
    'enter_new_pass_again' => 'Enter your New Password Again',

    'update' => 'Update',
    'refresh' => 'Refresh'
];
