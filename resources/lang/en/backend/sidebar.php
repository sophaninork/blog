<?php

return [
    //Sidebar Lang
    'dashboard' => 'Dashboard',
    'tag' => 'Tag',
    'category' => 'Category',
    'posts' => 'Posts',
    'pending_post' => 'Pending Post',
    'favorite_posts' => 'Favorite Posts',
    'comments' => 'Comments',
    'authors' => 'Authors',
    'subscribers' =>
    'Subscribers',
    'settings' => 'Settings',
    'logout' => 'Logout',
    'languages' => 'Languages',
    'profile' => 'Profile',
    'sign_out' => 'Sign Out'
];


