<?php
return [
    //Index Lang
    'add_new_post' => 'Add New Post',
    'all_posts' => 'All Posts',
    'id' => 'ID',
    'title' => 'Title',
    'author' => 'Author',
    'is_approved' => 'Is Approved',
    'status' => 'Status',
    'create_at' => 'Create At',
    'update_at' => 'Update At',
    'action' => 'Action',

    //create Lang
    'post_title' => 'Post Title',
    'featured_image' => 'Featured Image',
    'publish' => 'Publish',
    'back' => 'back',
    'save' => 'Save',
    'category_and_tags' => 'Categories and Tags',
    'select_category' => 'Select Category',
    'select_tag' => 'Select Tag',
    'content' => 'Content',
    'post_title_required' => 'Title field is required',
    'post_img_required' => 'Image field is required',
    'categories_required' => 'Please select category',
    'tags_required' => 'Please select tag',
    'content_required' => 'Content field is required',
    'post_create_success' => 'Post Success!',
    'approved' => 'Approved',
    'pending' => 'Pending',

    //edit Lang
    'edit_post' => 'Edit Post',
    'post_update_success' => 'Post update success!',

    //destroy lang
    'post_delete_success' => ''

];
