<?php
return [
    //subscriber lang
    'all_subscriber' => 'All Subscribers',
    'id' => 'ID',
    'email' => 'E-mail',
    'create_at' => 'Create At',
    'update_at' => 'Update At',
    'action' => 'Action',
    'delete_subscriber' => 'Subscriber Successfully Deleted'
];
