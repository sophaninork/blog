<?php
return [
    //profile lang

    'posts' => 'Posts',
    'favorite_posts' => 'Favorite Posts',
    'about_me' => 'About Me',
    'contact' => 'Contact',
    'username' => 'Username',
    'email' => 'E-mail',
    'tel' => 'Tel',
    'location' => 'Location',
    'bio' => 'Bio',
    'timeline' => 'Home',
    'shared_publicly' => 'Shared publicly'

];
