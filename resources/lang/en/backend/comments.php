<?php
return[
    //comments lang
    'all_comments' => 'All Comments',
    'comment_info' => 'Comment Info',
    'post_info' => 'Post Info',
    'action' => 'Action',
    'reply_comment' => 'Reply',
    'delete_comment' => 'Comment has been deleted!'
];
