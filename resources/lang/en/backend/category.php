<?php
return [
      //Index Category Lang
      'add_new_category' => 'Add New Category',
      'all_category' => 'All Category',
      'id' => 'ID',
      'name' => 'Name',
      'post_count' => 'Post Count',
      'create_at' => 'Create At',
      'update_at' => 'Update At',
      'action' => 'Action',

      //Create Category Lang
      'category_name' => 'Category Name',
      'back' => 'Back',
      'save' => 'Save',
      'category_name_required' => 'Category name field is required',
      'category_img_required' => 'Image field is required',
      'category_create_success' => 'Category create successfully',

      //Edit category Lang
      'edit_category' => 'Edit Category',
      'category_name' => 'Category Name',
      'category_update_success' => 'Category update successfully',

      //Destroy category lang
      'category_delete_success' => 'Category delete successfully',
];
