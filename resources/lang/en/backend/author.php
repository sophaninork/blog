<?php
return [
    //autjor lang
    'all_authors' => 'All Authors',
    'id' => 'ID',
    'name' => 'Name',
    'post' => 'Post',
    'comment' => 'Comments',
    'favorite_post' => 'Favorite Posts',
    'create_at' => 'Create At',
    'action' => 'Action',
    'delete_author' => 'Author has been delete successfully'
];
