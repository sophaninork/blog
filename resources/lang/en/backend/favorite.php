<?php
return [
    //favorite posts lang
    'all_favorite_post' => 'All Favorite Posts',
    'id' => 'ID',
    'title' => 'Title',
    'author' => 'Author',
    'action' => 'Action',
    'delete_success' => 'Has been remove from your favorite list!'
];
