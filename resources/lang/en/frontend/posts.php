<?php
return [
    //posts lang
    'all_posts' => 'All Posts',
    'add_to_fav' => 'Post successfully added to your favorite list :)',
    'remove_from_fav' => 'Post successfully removed from your favorite list :)',
    'login_before_action' => 'To add favorite list. You need to login first.',
    'share' => 'SHARE',
    'just_login_for_any_comment' => 'Just Login, for any comment.',
    'about_author' => 'ABOUT AUTHOR',
    'post_by' => 'Post by',
    'enter_your_comment' => 'Enter your comment',
    'email' => 'E-mail',
    'contact' => 'Contact',
    'post_date' => 'Post date',
    'categories' => 'CATEGORIES',
    'post_comment' => 'POST COMMENT',
    'comments' => 'COMMENTS',
    'no_comment_yet_be_the_first_comment' => 'No Comment yet, Be the first ?',

    'profile_author' => 'PROFILE AUTHOR',
    'username' => 'Username',
    'gender' => 'Gender',
    'male' => 'Male',
    'female' => 'Female',
    'address' => 'Address',
    'bio' => 'Bio',
    'author_since' => 'Author Since',
    'total_post' => 'Total Posts'
];
