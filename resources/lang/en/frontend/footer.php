<?php
return [
    //footer lang
    'language' => 'Language',
    'categories' => 'CATEGORIES',
    'subscribers' => 'SUBSCRIBERS',
    'enter_your_email' => 'Enter Your Email',
    'sub_success' => 'You Successfully added to our subscriber list :)',
    'email_sub_unique' => 'The email has already been taken!'
];
