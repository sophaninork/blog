@extends('layouts.frontend.app')

@section('title', 'Category')

@push('css')

    <link href="{{ asset('assets/frontend/css/category/styles.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/frontend/css/category/responsive.css') }}" rel="stylesheet">

    <style>
        .favorite_posts{
            color: blue;
        }
        .slider{
            height: 400px;
            width: 100%;
            background-size: cover;
            background-image: url({{ Storage::disk('public')->url('category/'.$category->image) }});
            
        }
        html {
            scroll-behavior: smooth;
        }
    </style>
@endpush

@section('content')
    <div class="slider display-table center-text">
        <h1 class="title display-table-cell"><b>{{ $category->name }}</b></h1>
    </div><!-- slider -->

    <section class="blog-area section">
        <div class="container" id="table_data">
            @include('article')
        </div><!-- container -->
    </section><!-- section -->
@endsection

@push('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $(document).on('click', '.pagination a', function(event){
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            fetch_data(page);
        });

        function fetch_data(page)
        {
            $.ajax({
                url:"/category/{slug}/fetch_data?page="+page,
                success: function(category)
                {
                    window.scrollTo(0, 0);
                    $('#table_data').html(category);
                }
            });
        }
    });
</script>
@endpush