	<header>
		<div class="container-fluid position-relative no-side-padding">

			<a href="{{ route('home') }}" class="logo">Blog</a>

			<div class="menu-nav-icon" data-nav-menu="#main-menu"><i class="ion-navicon"></i></div>

			<ul class="main-menu visible-on-click" id="main-menu">
				<li>
					<a href="{{ route('home') }}">{{__('frontend/navbar.home')}}</a>
				</li>
				<li>
					<a href="{{ route('post.index') }}">{{__('frontend/navbar.posts')}}</a>
				</li>
				@guest
				<li>
					<a href="{{ route('login') }}">{{__('frontend/navbar.login')}}</a>
				</li>
				@else
					@if(Auth::user()->role->id == 1)
						<li>
							<a href="{{ route('admin.dashboard') }}">{{__('frontend/navbar.dashboard')}}</a>
						</li>
					@endif
					@if(Auth::user()->role->id == 2)
						<li>
							<a href="{{ route('author.dashboard') }}">{{__('frontend/navbar.dashboard')}}</a>
						</li>
					@endif
				@endguest
			</ul>

			<div class="src-area">
            	<form method="GET" action="{{ route('search') }}">
            		<button class="src-btn" type="submit"><i class="ion-ios-search-strong"></i></button>
            		<input class="src-input" value="{{ isset($query) ? $query : '' }}"  name="query" type="text" placeholder="{{__('frontend/navbar.type_of_search')}}">
            	</form>
            </div>
		</div>
	</header>
