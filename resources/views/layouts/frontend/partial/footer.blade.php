
	<footer>

		<div class="container">
			<div class="row">

				<div class="col-lg-4 col-md-6">
					<div class="footer-section">

						{{-- <a class="logo" href="#"><img src="images/logo.png" alt="Logo Image"></a> --}}
						<p class="copyright">{{ config('app.name') }} @ 2020. All rights reserved.</p>
						<p class="copyright">Designed by <a href="https://colorlib.com" target="_blank">Colorlib</a> and Develop by <a href="https://web.facebook.com/profile.php?id=100006449919563" target="_blank">Sophanin Ork</a></p>
                            <strong>{{__('frontend/footer.language')}} : </strong>
                        <span>
                            <a href="/locale/kh" style="color: dodgerblue;">Khmer</a> |
                            <a href="/locale/en"style="color: dodgerblue;">English</a>
                        </span>
					</div><!-- footer-section -->
				</div><!-- col-lg-4 col-md-6 -->

				<div class="col-lg-4 col-md-6">
						<div class="footer-section">
						<h4 class="title"><b>{{__('frontend/footer.categories')}}</b></h4>
						<ul>
							@foreach ($categories as $category)
								<li><a href="{{ route('category.posts', $category->slug) }}">{{ $category->name }}</a></li>
							@endforeach
						</ul>
					</div><!-- footer-section -->
				</div><!-- col-lg-4 col-md-6 -->

				<div class="col-lg-4 col-md-6">
					<div class="footer-section">

						<h4 class="title"><b>{{__('frontend/footer.subscribers')}}</b></h4>
						<div class="input-area">
							<form method="POST" action="{{ route('subscriber.store') }}">
								@csrf
								<input class="email-input" name="email" type="email" placeholder="{{__('frontend/footer.enter_your_email')}}">
								<button class="submit-btn" type="submit"><i class="icon ion-ios-email-outline"></i></button>
							</form>
						</div>

					</div><!-- footer-section -->
				</div><!-- col-lg-4 col-md-6 -->

			</div><!-- row -->
		</div><!-- container -->
	</footer>
