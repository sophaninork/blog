@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>    
        <p>{{ $message }}</p>
    </div>
@endif

@if ($message = Session::get('delete'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>    
        <p>{{ $message }}</p>
    </div>
@endif


