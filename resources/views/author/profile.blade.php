@extends('layouts.backend.app')

@section('title','Profile')

@push('css')

@endpush

@section('content')
<div class="container-fluid">
      <div class="row clearfix">
          <div class="col-xs-12 col-sm-3">
              <div class="card profile-card">
                  <div class="profile-header">&nbsp;</div>
                  <div class="profile-body">
                      <div class="image-area">
                          <img src="{{ Storage::disk('public')->url('profile/'.Auth::user()->image) }}" width="136" height="136" alt="User">
                      </div>
                      <div class="content-area">
                          <h3>{{ Auth::user()->name }}</h3>
                          <p>{{ Auth::user()->role->name }}</p>
                      </div>
                  </div>
                  <div class="profile-footer">
                      <ul>
                          <li>
                              <span>{{__('backend/profile.posts')}}:</span>
                              <span>{{ Auth::user()->posts->count() }}</span>
                          </li>
                          <li>
                              <span>{{__('backend/profile.favorite_posts')}}:</span>
                              <span>{{ Auth::user()->favorite_posts->count() }}</span>
                          </li>
                      </ul>
                  </div>
              </div>

              <div class="card card-about-me">
                  <div class="header">
                      <h2>{{__('backend/profile.about_me')}}</h2>
                  </div>
                  <div class="body">
                      <ul>
                          <li>
                              <div class="title">
                                  <i class="material-icons">library_books</i>
                                  {{__('backend/profile.contact')}}
                              </div>
                              <div class="content">
                                  <table style="width: 90%">
                                      <tr>
                                          <td>{{__('backend/profile.username')}}:</td>
                                          <td>{{ Auth::user()->username }}</td>
                                      </tr>
                                      <tr>
                                          <td>{{__('backend/profile.email')}}:</td>
                                          <td>{{ Auth::user()->email }}</td>
                                      </tr>
                                      <tr>
                                          <td>{{__('backend/profile.tel')}}:</td>
                                          <td>{{ Auth::user()->phonenumber }}</td>
                                      </tr>
                                    </table>
                            </div>
                          </li>
                          <li>
                              <div class="title">
                                  <i class="material-icons">location_on</i>
                                  {{__('backend/profile.location')}}
                              </div>
                              <div class="content">
                                  {{ Auth::user()->address }}
                              </div>
                          </li>
                          <li>
                              <div class="title">
                                  <i class="material-icons">notes</i>
                                  {{__('backend/profile.bio')}}
                              </div>
                              <div class="content">
                                  {{ Auth::user()->bio }}
                              </div>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
          <div class="col-xs-12 col-sm-9">
              <div class="card">
                  <div class="body">
                      <div>
                          <ul class="nav nav-tabs" role="tablist">
                              <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">{{__('backend/profile.timeline')}}</a></li>
                          </ul>

                          @foreach ($posts as $post)
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="home">
                                <div class="panel panel-default panel-post">
                                    <div class="panel-heading">
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="#">
                                                    <img src="{{ Storage::disk('public')->url('profile/'.$post->user->image) }}">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">
                                                    <a href="#">{{$post->user->name}}</a>
                                                </h4>
                                                Shared publicly - {{$post->created_at->diffForHumans()}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="post">
                                            <div class="post-heading">
                                                <p>{{ $post->title }}</p>
                                            </div>
                                            <div class="post-content">
                                                <img class="img-responsive" src="{{ Storage::disk('public')->url('post/'.$post->image) }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <i class="material-icons">visibility</i>
                                                    <span>{{ $post->view_count }}</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="material-icons">comment</i>
                                                    <span>{{ $post->comments->count() }}</span>
                                                </a>
                                            </li>
                                            <li>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                        </div>
                          @endforeach


                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection

@push('js')

@endpush
