@extends('layouts.backend.app')

@section('title', 'Category')

@push('css')

@endpush

@section('content')
<div class="container-fluid">
    <!-- Vertical Layout | With Floating Label -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        {{ __('backend/category.add_new_category')  }}
                    </h2>
                </div>
                <div class="body">
                    <form action="{{ route('admin.category.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group form-float">
                            <div class="form-line">
                            <input type="text" id="name" class="form-control" name="name" value="{{ old('name') }}" autocomplete="off">
                                <label class="form-label">{{ __('backend/category.category_name')  }}</label>
                            </div>
                            @if ($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input type="file" name="image" value="{{ old('image') }}">
                            @if ($errors->has('image'))
                                <span class="text-danger">{{ $errors->first('image') }}</span>
                            @endif
                        </div>

                        <a class="btn btn-danger m-t-15 waves-effect" href="{{ route('admin.category.index') }}">{{ __('backend/category.back')  }}</a>
                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">{{ __('backend/category.save')  }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')

@endpush
