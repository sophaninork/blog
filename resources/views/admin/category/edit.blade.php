@extends('layouts.backend.app')

@section('title', 'Category')

@push('css')

@endpush

@section('content')
<div class="container-fluid">
    <!-- Vertical Layout | With Floating Label -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        {{ __('backend/category.edit_category')  }}
                    </h2>
                </div>
                <div class="body">
                    <form action="{{ route('admin.category.update', $category->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" id="name" class="form-control" name="name" value="{{ $category->name }}" autocomplete="off">
                                <label class="form-label">{{ __('backend/category.category_name')  }}</label>
                            </div>
                            <p style="color: red;">@error('name'){{ $message }}@enderror</p>
                        </div>

                        <div class="form-group">
                            <input type="file" name="image">
                        </div>

                        <a class="btn btn-danger m-t-15 waves-effect" href="{{ route('admin.category.index') }}">{{ __('backend/category.back')  }}</a>
                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">{{ __('backend/category.save')  }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')

@endpush
